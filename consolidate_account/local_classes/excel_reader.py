import xlrd
import xlwt
import datetime

import logging



logging.basicConfig(filename='log/LOG_' + datetime.datetime.today().strftime('%Y-%m-%d') + '.log',
                    format='%(levelname)s:%(asctime)s %(message)s',
                    level=logging.DEBUG)
logging.getLogger().addHandler(logging.StreamHandler())
log = logging.getLogger(__name__)\



class ExcelReader():
    BOOK = None

    def set_book(self, filename):
        try:
            self.BOOK = xlrd.open_workbook(filename)
            log.info('-------------- LOADING > %s' % filename)
        except Exception as ex:
            log.error(ex)
            print(ex)
            exit()

    @staticmethod
    def is_number(text):
        try:
            int(text)
            return True
        except:
            return False

    def page_name_valid(self, page_name):
        return len(page_name) >= 4 and self.is_number(page_name[0:4])

    def confirm_page_columns(self, result_filename):
        wb = xlwt.Workbook()
        new_page = wb.add_sheet('columns')
        rows = []
        row_number = 0
        for page in self.BOOK.sheets():
            if self.page_name_valid(page.name) is False:
                log.warning('SKIPPED: ' + page.name)
                continue
            log.info('CHECKING PAGE ' + page.name)
            new_page.write(row_number, 0, page.name)
            for col_number in range(page.ncols):
                head_cell_value = page.cell_value(1, col_number)
                new_page.write(row_number, col_number + 1, head_cell_value)
            row_number += 1
        wb.save(result_filename + '.xls')
        log.info('PROCESSED FILE destination: ' + result_filename + '.xls')

    def consolidate_pages(self, result_filename):
        logging.info('-------- Consolidate method called --------')
        date_format = xlwt.XFStyle()
        date_format.num_format_str = 'dd/mm/yyyy'

        wb = xlwt.Workbook()
        new_page = wb.add_sheet('CONSOLIDATE')
        new_row_number = 1

        for page in self.BOOK.sheets():
            page_counter = 0
            if self.page_name_valid(page.name) is False:
                # No valid page name
                log.warning('SKIPPED: ' + page.name)
                continue
            if new_row_number == 1:
                # get headers
                for header_column in range(page.ncols):
                    new_page.write(0, header_column, page.cell_value(1, header_column))
            # get rows
            for row_number in range(2, page.nrows):
                detalle_value = page.cell_value(row_number, 2)
                if 'saldo al' in str(detalle_value).lower().strip():
                    log.info('PAGE %s: ROW: %s | SKIPPED: %s' % (page.name, row_number, 'SALDO'))
                    continue
                if detalle_value == '':
                    log.info('PAGE %s: ROW: %s | SKIPPED: %s' % (page.name, row_number, 'EMPTY DETALLE'))
                    continue
                page_counter += 1
                # first cell as datetime
                date_cell = page.cell_value(rowx=row_number, colx=0)
                # if date_cell != '':
                #     cell_dt = datetime.datetime(*xlrd.xldate_as_tuple(date_cell, self.BOOK.datemode))
                #     new_page.write(new_row_number, 0, cell_dt.strftime('%d/%m/%Y'))
                new_page.write(new_row_number, 0, date_cell, date_format)
                for col_number in range(1, page.ncols):
                    cell_value = page.cell_value(row_number, col_number)
                    new_page.write(new_row_number, col_number, cell_value)
                new_row_number += 1
            log.info('PAGE %s: PROCESSED ROWS: %s ' % (page.name, page_counter))
            # print('Page ', page.name, 'takes', page_counter)

        wb.save(result_filename + '.xls')
