import sys
from datetime import datetime as dt
from local_classes.excel_reader import ExcelReader

if __name__ == '__main__':
    args = sys.argv
    excel_reader = ExcelReader()
    if not args[1]:
        print("ORIGIN FILENAME REQUIRED")
        exit()
    excel_reader.set_book(args[2])
    if args[1] == 'check':
        excel_reader.confirm_page_columns(args[3] if len(args)>=4 else 'check')
    elif args[1] == 'join':
        excel_reader.consolidate_pages(args[3] if len(args)>=4 else dt.now().strftime('JOIN_%Y-%m-%d'))
    else:
        print('NONE')